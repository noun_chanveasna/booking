<?php

namespace App\Traits;


trait RequestTrait {

    private function apiRequest($method,$parameters =[]) {
        $url = env('TELEGRAM_URL') . env('TELEGRAM_TOKEN') . '/' . $method;
        $handle = curl_init($url);
        curl_setopt($handle,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($handle,CURLOPT_CONNECTTIMEOUT,5);
        curl_setopt($handle,CURLOPT_TIMEOUT,60);
        $data = json_encode($parameters);
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );

        $response = curl_exec($handle);

        curl_close($handle);
        return $response;
    }
}