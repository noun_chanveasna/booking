<?php

namespace App\Http\Controllers;

use App\Traits\RequestTrait;


class TelegramController extends Controller
{

    use RequestTrait;

    private $button = [
        'get_post' => 'List Meeting Room',
    ];

    public function webhook()
    {
        return $this->apiRequest('setWebhook', [
            'url' => str_replace('http', 'https', url(route('webhook')))
        ]) ? ['success'] : ['something wrong'];
    }


    public function index() {
        $update = json_decode(file_get_contents('php://input'));

        $months = [
            1 => 'Jan',
            2 => 'Feb',
            3 => 'Mar',
            4 => 'Apr',
            5 => 'May',
            6 => 'Jun',
            7 => 'Jul',
            8 => 'Aug',
            9 => 'Sep',
            10 => 'Oct',
            11 => 'Nov',
            12 => 'Dec'
        ];

        $month1 = $month2 = $month3 = [];

        foreach ($months as $key => $row) {
            if ($key > 0 && $key <= 4) {
                $month1[] = array('text' => $row, 'callback_data' => $key);
            } elseif ($key > 4 && $key <= 8) {
                $month2[] = array('text' => $row, 'callback_data' => $key);
            } else {
                if ($key < 10) {
                    $key = '0' . $key;
                }
                $month3[] = array('text' => $row, 'callback_data' => $key);
            }
        }

        $keyboard = array(
            'inline_keyboard' => array_merge(array($month1), array($month2), array($month3))
        );

        if (! empty($update->callback_query->data)) {
            $param = [
                'chat_id' => $update->callback_query->from->id,
                'text' => 'test',
                'parse_mode' => 'html',
                'reply_markup' => json_encode($keyboard)
            ];

            return file_get_contents(env('TELEGRAM_URL') . env('TELEGRAM_TOKEN') . '/sendMessage?' . http_build_query($param));
        }

        return false;
    }
}
