<?php

namespace App\Http\Controllers;

use App\booking;
use App\room;
use App\Traits\RequestTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class BookingController extends Controller {

    use RequestTrait;

    const TODAY = 'today';
    const TOMORROW = 'tomorrow';

    /**
     * @var array $months
     */
    protected $months = [
        1 => 'Jan',
        2 => 'Feb',
        3 => 'Mar',
        4 => 'Apr',
        5 => 'May',
        6 => 'Jun',
        7 => 'Jul',
        8 => 'Aug',
        9 => 'Sep',
        10 => 'Oct',
        11 => 'Nov',
        12 => 'Dec'
    ];

    /**
     * @var array $times
     */
    protected $times = [
        1 => '08:00',
        2 => '08:30',
        3 => '09:00',
        4 => '09:30',
        5 => '10:00',
        6 => '10:30',
        7 => '11:00',
        8 => '11:30',
        9 => '12:00',
        10 => '12:30',
        11 => '13:00',
        12 => '13:30',
        13 => '14:00',
        14 => '14:30',
        15 => '15:00',
        16 => '15:30',
        17 => '16:00',
        18 => '16:30',
        19 => '17:00',
        20 => '17:30'
    ];


    public function setWebhook(Request $request) {
        $response = $this->apiRequest('setWebhook', ['url' => $request->query('url')]);
        return $response;
    }


    /**
     * @param $id
     * @return mixed
     */
    public function typing($id) {
        return $this->apiRequest('sendChatAction', ['chat_id' => $id, 'action' => 'typing']);
    }


    /**
     * @param $arr
     * @param $row
     * @param $continue
     * @param $id
     * @param $id_back
     * @param $text
     * @return array
     */
    public function setInlineKeyboard($arr, $row, $continue, $id, $id_back, $text) {
        $count = explode(',', $id_back);
        $count_arr = count($count);

        $myArray = array();
        $month = array();
        $count = 1;
        foreach ($arr as $key => $item) {
            if ($key < $continue) {
                continue;
            }
            $month[] = array('text' => $item, 'callback_data' => "$id,$key");
            if ($count % $row == 0) {
                $myArray[] = $month;
                $month = array();
            }

            $count++;
        }

        if (count($month) > 0) {
            $myArray[] = $month;
        }

        $month = array();

        if ($count_arr != 2 | $count_arr != 1) {
            $month[] = array('text' => '⬅ Return to room', 'callback_data' => $text);
            $month[] = array('text' => '↩ Back', 'callback_data' => $id_back);

        } else {
            $month[] = array('text' => '↩ Back', 'callback_data' => $text);

        }

        $myArray[] = $month;

        $keyboard = array(
            'inline_keyboard' => $myArray
        );

        return $keyboard;

    }


    /**
     * @param int $id
     *
     * @return mixed
     */
    public function room_name($id) {
        $room = room::where('id', $id)->first()->name;
        return $room;
    }

    //Month
    public function month($chat_id, $id, $id_back) {
        $now = Carbon::now()->month;
        $current_date = date('Y-M-d');
        $room = $this->room_name($id[2]);
        $this->apiRequest(

            'editMessageText',
            [
                'chat_id' => $chat_id->from->id,
                'message_id' => $chat_id->message->message_id,
                'text' => "Current booking (<b>$current_date</b>):\n\n- Room: <b>$room</b>\n\n<b>Please select the following months:</b>",
                'parse_mode' => 'html',
                'reply_markup' => json_encode($this->setInlineKeyboard($this->months, 4, $now, $id = "$id[0],$id[1],$id[2]", $id_back = "$id_back[0],$id_back[1]", 'booking'))

            ]

        );

    }

    /**
     * @param $chat_id
     * @param $id
     * @param $id_back
     */
    public function date($chat_id, $id, $id_back) {
        $arr_days = array();
        $year = Carbon::now()->year;
        $month = Carbon::now()->month;
        $day = Carbon::now()->day;
        $current_date = date('Y-M-d');
        $room = $this->room_name($id[2]);
        $day_of_month = cal_days_in_month(CAL_GREGORIAN, $id[3], $year);

        for ($i = 1; $i <= $day_of_month; $i++) {
            if ($month == $id[3] && $day > $i) {
                continue;
            }

            $arr_days[$i] = sprintf("%'.02d", $i);

        }


        $this->apiRequest(

            'editMessageText',
            [
                'chat_id' => $chat_id->from->id,
                'message_id' => $chat_id->message->message_id,
                'text' => "Current booking (<b>$current_date</b>):\n\n- Room: <b>$room</b>\n\n<b>Please select the following date :</b>",
                'parse_mode' => 'html',
                'reply_markup' => json_encode($this->setInlineKeyboard($arr_days, 7, null, $id = "$id[0],$id[1],$id[2],$id[3]", $id_back = "$id_back[0],$id_back[1],$id_back[2]", 'booking'))

            ]

        );

    }

    //Start Time
    public function start_time($chat_id, $id, $id_back, $message, $text) {
        $year = Carbon::now()->year;
        $date = "$year-$id[3]-$id[4]";
        $check_time = DB::select("select time(start_datetime) as start_time,time(end_datetime) as end_time FROM bookings WHERE room_id = :id AND DATE(start_datetime) = :date ORDER BY start_time ASC", ['id' => $id[2], 'date' => $date]);

        $c_index = 0;
        $tem = array();
        $key_start = null;
        $key_end = null;

        foreach ($this->times as $key => $item) {

            if ($c_index < count($check_time)) {

                $start_time_str = substr($check_time[$c_index]->start_time, 0, -3);
                if ($item == $start_time_str) {
                    $key_start = $key;
                    $key_end = null;
                }

                $end_time_str = substr($check_time[$c_index]->end_time, 0, -3);
                if ($item == $end_time_str) {
                    $key_end = $key;
                    $c_index++;
                }


            }

            if ($c_index < count($check_time)) {

                $start_time_str = substr($check_time[$c_index]->start_time, 0, -3);
                if ($item == $start_time_str) {
                    $key_start = $key;
                    $key_end = null;
                }

            }

            if ($key_start == null || (($key_start != null && $key < $key_start) || ($key_end != null && $key >= $key_end))) {
                $tem[$key] = $item;
            }

        }

        $room = $this->room_name($id[2]);
        $year = Carbon::now()->year;
        $month = $this->months[$id[3]];
        $day = sprintf("%'.02d", $id[4]);
        $current_date = date('Y-M-d');
        $send_message = "";

        $array_time = array();
        foreach ($tem as $key => $item) {
            $array_time[$key] = date("H:i A", strtotime($item));
        }

        if (isset($id_back[3])) {
            $return_id = "$id_back[0],$id_back[1],$id_back[2],$id_back[3]";
        } else {
            $return_id = $id_back;
        }

        if ($message != null) {
            $send_message = $message;
        }

        $this->apiRequest(

            'editMessageText',
            [
                'chat_id' => $chat_id->from->id,
                'message_id' => $chat_id->message->message_id,
                'text' => "Current booking (<b>$current_date</b>):\n\n- Room: <b>$room</b>\n- Date: <b>$year-$month-$day</b> \n\n$send_message<b>Please select start time : </b>",
                'parse_mode' => 'html',
                'reply_markup' => json_encode($this->setInlineKeyboard($array_time, 4, null, $id = "$id[0],$id[1],$id[2],$id[3],$id[4]", $return_id, $text))

            ]

        );
    }

    //End Time
    public function end_time($chat_id, $id, $id_back) {
        $year = Carbon::now()->year;
        $date = "$year-$id[3]-$id[4]";
        $start = $this->times[$id[5]];
        $check_time = DB::select("select time(start_datetime) as start_time,time(end_datetime) as end_time FROM bookings WHERE room_id = :id AND DATE(start_datetime) = :date ORDER BY start_time ASC", ['id' => $id[2], 'date' => $date]);

        $c_index = 0;
        $tem = array();
        $key_start = null;
        $key_end = null;
        foreach ($this->times as $key => $item) {

            if ($c_index < count($check_time)) {
                $start_time_str = substr($check_time[$c_index]->start_time, 0, -3);
                if ($item == $start_time_str) {
                    $key_start = $key;
                    $c_index++;
                }

            }

            if ($id[5] < $key) {
                $tem[$key] = $item;

                if ($key_start >= $key) {
                    break;
                }

            }


        }


        $room = $this->room_name($id[2]);
        $year = Carbon::now()->year;
        $month = $this->months[$id[3]];
        $day = sprintf("%'.02d", $id[4]);

        $selected_start = date("H:i A", strtotime($start));
        $current_date = date('Y-M-d');

        $this->apiRequest(

            'editMessageText',
            [
                'chat_id' => $chat_id->from->id,
                'message_id' => $chat_id->message->message_id,
                'text' => "Current booking (<b>$current_date</b>) :\n\n- Room: <b>$room</b>\n- Date: <b>$year-$month-$day</b>\n- Start: <b>$selected_start</b>\n\n<b>Please select end time :</b>",
                'parse_mode' => 'html',
                'reply_markup' => json_encode($this->setInlineKeyboard($tem, 4, null, $id = "$id[0],$id[1],$id[2],$id[3],$id[4],$id[5]", $id_back = "$id_back[0],$id_back[1],$id_back[2],$id_back[3],$id_back[4]", 'booking'))

            ]

        );
    }


    //Cancel
    public function cancel($chat_id, $id, $text) {
        $keyboard = '';
        $booked = booking::where('user_id', $chat_id)->get();
        $cancel_booking = array();
        $booking = "";
        foreach ($booked as $item) {
            $room_name = $item->room->name;
            $booking .= "No: <b>$item->id</b>\nRoom: <b>$room_name</b>\nStart: <b>$item->start_datetime</b>\nEnd: <b>$item->end_datetime</b> \n\n";
            $cancel_booking[] .= $item->id;
            $myArray = array();
            $room = array();
            $count = 1;
            foreach ($cancel_booking as $row) {

                $room[] = array('text' => "No: $row", 'callback_data' => "cancel,$row");
                if ($count % 4 == 0) {
                    $myArray[] = $room;
                    $room = array();
                }

                $count++;
            }
            if (count($room) > 0) {
                $myArray[] = $room;
            }
            $room = array();
            $myArray[] = $room;

            $keyboard = array(
                'inline_keyboard' => $myArray
            );
        }

        if ($id != null) {
            $message_id[] = ['message_id' => $id];
        } else {
            $message_id = "";
        }

        $this->apiRequest(

            "$text",
            [
                'chat_id' => $chat_id,
                $message_id,
                'text' => "My Bookings:\n\n" . $booking . "<b>Please click number room which u want to cancel</b>",
                'parse_mode' => 'html',
                'reply_markup' => json_encode($keyboard)

            ]

        );
    }


    public function index() {
        $tg = json_decode(file_get_contents('php://input'));

        $all_room = room::all();
        $arr_key = array();

        foreach ($all_room as $key => $rooms) {

            $arr_key[] .= $rooms->name;
            $myArray = array();
            $room = array();
            $count = 1;
            foreach ($arr_key as $key => $row) {
                $key++;
                $room[] = array('text' => "🏢$row", 'callback_data' => "booking,$key");
                if ($count % 1 == 0) {
                    $myArray[] = $room;
                    $room = array();
                }

                $count++;
            }
            if (count($room) > 0) {
                $myArray[] = $room;
            }
            $room = array();
            $myArray[] = $room;

            $keyboard = array(
                'inline_keyboard' => $myArray
            );

        }

        if (isset($tg->message->new_chat_member->id)) {

            $check_user = User::where('user_id', $tg->message->new_chat_member->id)->first();
            $count_user = count($check_user);
            $first_name = $tg->message->new_chat_member->first_name;
            $last_name = $tg->message->new_chat_member->last_name;

            if ($count_user == true) {
                $check_user->status = 1;
                $check_user->save();
            } else {

                $user = new User;
                $user->first_name = $first_name;
                if (isset($tg->message->new_chat_member->last_name)) {
                    $user->last_name = $last_name;
                } else {
                    $user->last_name = "";
                }

                if (isset($tg->message->new_chat_member->username)) {
                    $user->user_name = $tg->message->new_chat_member->username;
                } else {
                    $user->user_name = "";
                }

                $user->user_id = $tg->message->new_chat_member->id;
                $user->group_id = $tg->message->chat->id;
                $user->status = 1;
                $user->save();

                $this->apiRequest(
                    'sendMessage',
                    [
                        'chat_id' => $tg->message->chat->id,
                        'text' => "Hi there, $first_name $last_name, since you are added to telegram group,you are automatically able to use SINET BOOKING BOT (http://telegram.me/chanveasna_bot)",
                        'parse_mode' => 'html',

                    ]

                );

            }


        }

        if (isset($tg->message->left_chat_member->id)) {
            $user = User::where('user_id', $tg->message->left_chat_member->id)->first();
            $user->status = 0;
            $user->save();
        }

        if (isset($tg->message->text)) {

            if ($tg->message->text == '/start' | $tg->message->text == '/list') {

                $this->apiRequest(
                    'deleteMessage',
                    [
                        'chat_id' => $tg->message->chat->id,
                        'message_id' => $tg->message->message_id,

                    ]
                );
                $this->apiRequest(
                    'sendMessage',
                    [
                        'chat_id' => $tg->message->chat->id,
                        'text' => "<b>Please book the following rooms : </b>",
                        'parse_mode' => 'html',
                        'reply_markup' => json_encode($keyboard)

                    ]

                );

            }

            if ($tg->message->text == '/mybooking') {

                $user_id = $tg->message->chat->id;
                $my_booking = booking::where('user_id', $user_id)->get();
                $booking = "";

                if ($my_booking != null) {

                    foreach ($my_booking as $item) {
                        $room_name = $item->room->name;
                        $booking .= "Room: <b>$room_name</b>\nStart: <b>$item->start_datetime</b>\nEnd: <b>$item->end_datetime</b>\nStatus: <b>Active</b> \n\n";
                    }

                } else {
                    $booking = "You not yet booking\nPlease click /list for booking our room";
                }

                $this->apiRequest(

                    'sendMessage',
                    [
                        'chat_id' => $tg->message->chat->id,
                        'text' => "My Bookings:\n\n$booking Thank for your booking 😀",
                        'parse_mode' => 'html',

                    ]

                );


            }

            if ($tg->message->text == '/cancel') {
                sleep(0.5);

                $this->cancel($tg->message->chat->id, null, 'sendMessage');

            }

        }


        //Callback Data
        if (isset($tg->callback_query->data)) {
            $current_day = Carbon::now()->day;
            $current_month = Carbon::now()->month;
            $year = Carbon::now()->year;

            $chat_id = $tg->callback_query;
            $id = explode(',', $tg->callback_query->data);
            $count_arr = count($id);

            if ($id[0] == 'booking') {
                if ($count_arr == 1) {
                    $this->typing($chat_id->from->id);
                    $this->apiRequest(

                        'editMessageText',
                        [
                            'chat_id' => $chat_id->from->id,
                            'message_id' => $chat_id->message->message_id,
                            'text' => "<b>Please book the following rooms :</b>",
                            'parse_mode' => 'html',
                            'reply_markup' => json_encode($keyboard)

                        ]

                    );
                }

                //Month
                if ($count_arr == 2) {

                    $tmr = $current_day + 1;
                    $this->typing($chat_id->from->id);
                    $this->apiRequest(

                        'editMessageText',
                        [
                            'chat_id' => $chat_id->from->id,
                            'message_id' => $chat_id->message->message_id,
                            'text' => "Please select the following:\n<b>Today / Tomorrow / Months</b>",
                            'parse_mode' => 'html',
                            'reply_markup' => json_encode(
                                ['inline_keyboard' =>
                                    [
                                        [
                                            ['text' => '📆 Today', 'callback_data' => "today,$id[1],$id[1],$current_month,$current_day"],
                                            ['text' => '📆 Tomorrow', 'callback_data' => "tomorrow,$id[1],$id[1],$current_month,$tmr"],
                                            ['text' => '📆 Months', 'callback_data' => "booking,$id[1],$id[1]"],
                                        ],
                                        [
                                            ['text' => '↩ Back', 'callback_data' => 'booking'],
                                        ]

                                    ]
                                ]
                            )

                        ]

                    );

                }

                if ($count_arr == 3) {
                    $this->typing($chat_id->from->id);
                    $this->month($chat_id, $id, $id);
                }

                //Day
                if ($count_arr == 4) {

                    $this->typing($chat_id->from->id);
                    $this->date($chat_id, $id, $id);

                }


                //StartTime
                if ($count_arr == 5) {

                    $this->typing($chat_id->from->id);
                    $this->start_time($chat_id, $id, $id, null, 'booking');


                }


                //EndTime
                if ($count_arr == 6) {

                    $this->typing($chat_id->from->id);
                    $this->end_time($chat_id, $id, $id);

                }


                // Booking Room
                if ($count_arr == 7) {

                    $room = $this->room_name($id[2]);
                    $month = $this->months[$id[3]];
                    $day = sprintf("%'.02d", $id[4]);
                    $startTime = $this->times[$id[5]];
                    $endTime = $this->times[$id[6]];
                    $start_datetime = $year . "-" . $id[3] . "-" . $day . " " . $startTime;
                    $end_datetime = $year . "-" . $id[3] . "-" . $day . " " . $endTime;

                    $start = date("H:i A", strtotime($startTime));
                    $end = date("H:i A", strtotime($endTime));

                    $booked = booking::whereRaw('room_id =:id and (:start_datetime between (start_datetime+INTERVAL 1 MINUTE) and (end_datetime-INTERVAL 1 MINUTE) or :end_datetime between (start_datetime+INTERVAL 1 MINUTE) and (end_datetime-INTERVAL 1 MINUTE) or (start_datetime+INTERVAL 1 MINUTE) between :start_datetime1 and :end_datetime1 or (end_datetime-INTERVAL 1 MINUTE) between :start_datetime2 and :end_datetime2)', ['id' => $id[1], 'start_datetime' => $start_datetime, 'end_datetime' => $end_datetime, 'start_datetime1' => $start_datetime, 'end_datetime1' => $end_datetime, 'start_datetime2' => $start_datetime, 'end_datetime2' => $end_datetime])->get();

                    $check_booking = count($booked) > 0;

                    if ($check_booking) {
                        $busy_room = booking::whereRaw('room_id =:id and date(start_datetime)=:start_date', ['id' => $id[2], 'start_date' => $year . "-" . $id[3] . "-" . $day])->orderby('start_datetime')->get();
                        $not_available = "\nTime has users booked as below :\n";
                        foreach ($busy_room as $item) {
                            $start_time = date("H:i A", strtotime($item->start_datetime));
                            $end_time = date("H:i A", strtotime($item->end_datetime));
                            $not_available .= "- <b>" . $start_time . " to " . $end_time . "</b>\n";
                        }

                        $this->typing($chat_id->from->id);
                        $text = "Note* : <b>Your booking time has other user booked already!!!\n</b>$not_available\n";
                        $this->start_time($chat_id, $id, $id, $text, 'booking');

                    } else {

                        $booking = new booking;
                        $booking->room_id = $id[2];
                        $booking->start_datetime = "$year-$id[3]-$id[4] $startTime";
                        $booking->end_datetime = "$year-$id[3]-$id[4] $endTime";
                        $booking->user_id = $chat_id->from->id;
                        $booking->save();

                        $this->typing($chat_id->from->id);
                        $this->apiRequest(

                            'editMessageText',
                            [
                                'chat_id' => $chat_id->from->id,
                                'message_id' => $chat_id->message->message_id,
                                'text' => "<b>You have booked one room successfully.</b>\n\nRoom: <b>$room</b>\nDate: <b>$year-$month-$day</b>\nStart: <b>$start</b>\nEnd: <b>$end</b>\n\nThank for booking our room",
                                'parse_mode' => 'html',
                            ]

                        );

                    }

                }
            }


            if ($id[0] == self::TODAY) {

                if ($count_arr == 4) {
                    $tmr = $current_day + 1;
                    $this->typing($chat_id->from->id);
                    $this->apiRequest(

                        'editMessageText',
                        [
                            'chat_id' => $chat_id->from->id,
                            'message_id' => $chat_id->message->message_id,
                            'text' => "Please select the following:\n<b>Today / Tomorrow / Months</b>",
                            'parse_mode' => 'html',
                            'reply_markup' => json_encode(
                                ['inline_keyboard' =>
                                    [
                                        [
                                            ['text' => '📆 Today', 'callback_data' => "today,$id[1],$id[1],$current_month,$current_day"],
                                            ['text' => '📆 Tomorrow', 'callback_data' => "tomorrow,$id[1],$id[1],$current_month,$tmr"],
                                            ['text' => '📆 Months', 'callback_data' => "booking,$id[1],$id[1]"],
                                        ],
                                        [
                                            ['text' => '↩ Back', 'callback_data' => 'booking'],
                                        ]

                                    ]
                                ]
                            )

                        ]

                    );
                }

                if ($count_arr == 5) {
                    $this->typing($chat_id->from->id);
                    $this->start_time($chat_id, $id, $id, null, 'booking');
                }

                if ($count_arr == 6) {
                    $this->typing($chat_id->from->id);
                    $this->end_time($chat_id, $id, $id);
                }

                if ($count_arr == 7) {
                    $room = $this->room_name($id[2]);
                    $month = $this->months[$id[3]];
                    $day = sprintf("%'.02d", $id[4]);
                    $startTime = $this->times[$id[5]];
                    $endTime = $this->times[$id[6]];
                    $start_datetime = $year . "-" . $id[3] . "-" . $day . " " . $startTime;
                    $end_datetime = $year . "-" . $id[3] . "-" . $day . " " . $endTime;

                    $start = date("H:i A", strtotime($startTime));
                    $end = date("H:i A", strtotime($endTime));

                    $booked = booking::whereRaw('room_id =:id and (:start_datetime between (start_datetime+INTERVAL 1 MINUTE) and (end_datetime-INTERVAL 1 MINUTE) or :end_datetime between (start_datetime+INTERVAL 1 MINUTE) and (end_datetime-INTERVAL 1 MINUTE) or (start_datetime+INTERVAL 1 MINUTE) between :start_datetime1 and :end_datetime1 or (end_datetime-INTERVAL 1 MINUTE) between :start_datetime2 and :end_datetime2)', ['id' => $id[1], 'start_datetime' => $start_datetime, 'end_datetime' => $end_datetime, 'start_datetime1' => $start_datetime, 'end_datetime1' => $end_datetime, 'start_datetime2' => $start_datetime, 'end_datetime2' => $end_datetime])->get();

                    $check_booking = count($booked) > 0;

                    if ($check_booking) {
                        $busy_room = booking::whereRaw('room_id =:id and date(start_datetime)=:start_date', ['id' => $id[2], 'start_date' => $year . "-" . $id[3] . "-" . $day])->orderby('start_datetime')->get();
                        $not_available = "\nTime has users booked as below :\n";
                        foreach ($busy_room as $item) {
                            $start_time = date("H:i A", strtotime($item->start_datetime));
                            $end_time = date("H:i A", strtotime($item->end_datetime));
                            $not_available .= "- <b>" . $start_time . " to " . $end_time . "</b>\n";
                        }

                        $this->typing($chat_id->from->id);
                        $text = "Note* : <b>Your booking time has other user booked already!!!\n</b>$not_available\n";
                        $this->start_time($chat_id, $id, $id, $text, 'booking');

                    } else {

                        $booking = new booking;

                        $booking->room_id = $id[2];
                        $booking->start_datetime = "$year-$id[3]-$id[4] $startTime";
                        $booking->end_datetime = "$year-$id[3]-$id[4] $endTime";
                        $booking->user_id = $chat_id->from->id;
                        $booking->save();

                        $this->typing($chat_id->from->id);
                        $this->apiRequest(

                            'editMessageText',
                            [
                                'chat_id' => $chat_id->from->id,
                                'message_id' => $chat_id->message->message_id,
                                'text' => "<b>Your booking successful!!!</b>\n\nRoom: <b>$room</b>\nDate: <b>$year-$month-$day</b>\nStart: <b>$start</b>\nEnd: <b>$end</b>\n\nThank for booking our room",
                                'parse_mode' => 'html',
                            ]

                        );

                    }
                }


            }

            if ($id[0] == 'tomorrow') {
                if ($count_arr == 4) {
                    $tmr = $current_day + 1;
                    $this->typing($chat_id->from->id);
                    $this->apiRequest(

                        'editMessageText',
                        [
                            'chat_id' => $chat_id->from->id,
                            'message_id' => $chat_id->message->message_id,
                            'text' => "Please select the following:\n<b>Today / Tomorrow / Months</b>",
                            'parse_mode' => 'html',
                            'reply_markup' => json_encode(
                                ['inline_keyboard' =>
                                    [
                                        [
                                            ['text' => '📆 Today', 'callback_data' => "today,$id[1],$id[1],$current_month,$current_day"],
                                            ['text' => '📆 Tomorrow', 'callback_data' => "tomorrow,$id[1],$id[1],$current_month,$tmr"],
                                            ['text' => '📆 Months', 'callback_data' => "booking,$id[1],$id[1]"],
                                        ],
                                        [
                                            ['text' => '↩ Back', 'callback_data' => 'booking'],
                                        ]

                                    ]
                                ]
                            )

                        ]

                    );
                }

                if ($count_arr == 5) {
                    $this->typing($chat_id);
                    $this->start_time($chat_id, $id, $id, null, 'booking');
                }

                if ($count_arr == 6) {
                    $this->typing($chat_id->from->id);
                    $this->end_time($chat_id, $id, $id);
                }

                if ($count_arr == 7) {
                    $room = $this->room_name($id[2]);
                    $month = $this->months[$id[3]];
                    $day = sprintf("%'.02d", $id[4]);
                    $startTime = $this->times[$id[5]];
                    $endTime = $this->times[$id[6]];
                    $start_datetime = $year . "-" . $id[3] . "-" . $day . " " . $startTime;
                    $end_datetime = $year . "-" . $id[3] . "-" . $day . " " . $endTime;

                    $start = date("H:i A", strtotime($startTime));
                    $end = date("H:i A", strtotime($endTime));

                    $booked = booking::whereRaw('room_id =:id and (:start_datetime between (start_datetime+INTERVAL 1 MINUTE) and (end_datetime-INTERVAL 1 MINUTE) or :end_datetime between (start_datetime+INTERVAL 1 MINUTE) and (end_datetime-INTERVAL 1 MINUTE) or (start_datetime+INTERVAL 1 MINUTE) between :start_datetime1 and :end_datetime1 or (end_datetime-INTERVAL 1 MINUTE) between :start_datetime2 and :end_datetime2)', ['id' => $id[1], 'start_datetime' => $start_datetime, 'end_datetime' => $end_datetime, 'start_datetime1' => $start_datetime, 'end_datetime1' => $end_datetime, 'start_datetime2' => $start_datetime, 'end_datetime2' => $end_datetime])->get();

                    $check_booking = count($booked) > 0;

                    if ($check_booking) {
                        $busy_room = booking::whereRaw('room_id =:id and date(start_datetime)=:start_date', ['id' => $id[2], 'start_date' => $year . "-" . $id[3] . "-" . $day])->orderby('start_datetime')->get();
                        $not_available = "\nTime has users booked as below :\n";
                        foreach ($busy_room as $item) {
                            $start_time = date("H:i A", strtotime($item->start_datetime));
                            $end_time = date("H:i A", strtotime($item->end_datetime));
                            $not_available .= "- <b>" . $start_time . " to " . $end_time . "</b>\n";
                        }

                        $this->typing($chat_id->from->id);
                        $text = "Note* : <b>Your booking time has other user booked already!!!\n</b>$not_available\n";
                        $this->start_time($chat_id, $id, $id, $text, 'booking');

                    } else {

                        $booking = new booking;

                        $booking->room_id = $id[2];
                        $booking->start_datetime = "$year-$id[3]-$id[4] $startTime";
                        $booking->end_datetime = "$year-$id[3]-$id[4] $endTime";
                        $booking->user_id = $chat_id->from->id;
                        $booking->save();

                        $this->typing($chat_id->from->id);
                        $this->apiRequest(

                            'editMessageText',
                            [
                                'chat_id' => $chat_id->from->id,
                                'message_id' => $chat_id->message->message_id,
                                'text' => "<b>You have booked one room successfully.</b>\n\nRoom: <b>$room</b>\nDate: <b>$year-$month-$day</b>\nStart: <b>$start</b>\nEnd: <b>$end</b>\n\nThank for booking our room",
                                'parse_mode' => 'html',
                            ]

                        );

                    }
                }
            }

            if ($id[0] == 'cancel') {

                if ($count_arr == 2) {
                    $check_room = booking::where('id', $id[1])->first();
                    $room = $check_room->room->name;
                    $start_time = date("H:i A", strtotime($check_room->start_datetime));
                    $end_time = date("H:i A", strtotime($check_room->end_datetime));
                    $date = date("Y-M-d");
                    $delete = booking::where('id', $id[1])->delete();

                    if ($delete) {
                        $this->typing($chat_id->from->id);

                        return $this->apiRequest(

                            'editMessageText',
                            [
                                'chat_id' => $chat_id->from->id,
                                'message_id' => $chat_id->message->message_id,
                                'text' => "<b>You have cancelled the following room successfully\nNo : $check_room->id \n- Room : $room\n- Date :$date\n- Start time: $start_time \n- End time: $end_time </b>",
                                'parse_mode' => 'html',
                                'reply_markup' => json_encode(
                                    ['inline_keyboard' =>
                                        [
                                            [
                                                [
                                                    'text' => '<< Return to room', 'callback_data' => 'booking'
                                                ]
                                            ]

                                        ]
                                    ]
                                )

                            ]

                        );
                    }

                }

            }
        }

    }

}
